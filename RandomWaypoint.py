import random
import math
''''
Script to generate a Contact Plan for DTNSim based in the following movement model:
    1) Nodes start in random posicion inside a rectangle.
    2) Nodes chose a point to move, and travel to there with a random choosed speed (between certainly boundaries values)
    3) Nodes arrive to the destined place, wait a random choose time (between certainly boundaries values) and it start with 2)
    again.
'''
'''
#PARAMETERS
AMOUNT_OF_NODES = 4
END_TIME = 5000
ANTENNA_SCOPE = 5
TIME_STEP = 1
SEED = 24

#WORLD SIZE
WIDTH = 300
HEIGHT = 200

MAX_SPEED = 1.5
MIN_SPEED = 0.5

MIN_WAIT = 0
MAX_WAIT = 0
'''
OUTPUT_PATH = "/home/nando/Desktop/OneSetting/cp"
class Coord:

    def __init__(self,x,y):
        self.x = x
        self.y = y

    def distance(self,other):
        dx = self.x - other.x
        dy = self.y - other.y

        return math.sqrt(math.pow(dx,2) + math.pow(dy,2))

    def translate(self,dx,dy):
        self.x += dx
        self.y += dy

    @staticmethod
    def getRandomCoord(maxX,maxY):
        x = maxX * random.random()
        y = maxY * random.random()
        return Coord(x,y)

    def __str__( self ):
        return "Coord(%f,%f)"%(self.x,self.y)

'''
******************************************************
AGENT CLASS
******************************************************
'''
class Agent:
    def __init__(self, coord, width, height, min_speed, max_speed, min_wait, max_wait):
        self.coord = coord
        self.wait = 0
        self.speed = 0

        self.min_wait = min_wait
        self.max_wait = max_wait
        self.min_speed = min_speed
        self.max_speed = max_speed
        self.width = width
        self.height = height

    def move(self, ts):
        if self.wait > 0:
            #it was waiting
            self.wait -= ts
            if self.wait <= 0:
                #it was waiting, now it need to choose new position and avance
                self.setRandomDestinationAndSpeed()
                self.wait = 0 #Doesn't wait anymore
            return

        #It is moving
        possibleMovement = ts * self.speed
        distance = self.coord.distance(self.destination)

        if possibleMovement >= distance:
            #It has arrived to destination, update position
            self.coord = self.destination
            self.setRandomWait()
            if self.wait == 0:
                self.setRandomDestinationAndSpeed()
        else:
            #It hasn't arrived to destination, update position
            dx = (possibleMovement/distance) * (self.destination.x - self.coord.x)
            dy = (possibleMovement/distance) * (self.destination.y - self.coord.y)
            self.coord.translate(dx,dy)


    def setRandomDestination(self):
        self.destination = Coord.getRandomCoord(self.width, self.height)

    def setRandomSpeed(self):
        self.speed = (self.max_speed - self.min_speed) * random.random() + self.max_speed

    def setRandomWait(self):
        self.wait = (self.max_wait - self.min_wait) * random.random() + self.min_wait

    def setRandomDestinationAndSpeed(self):
        self.setRandomDestination()
        self.setRandomSpeed()

    def __str__( self ):
        return "Agent(At: %s, To: %s, Speed: %f, Wait: %f)"%(self.coord,self.destination,self.speed,self.wait)

'''
******** END AGENT CLASS *********
'''

'''
def getRandomCoord():
    return (WIDTH * random.random, HEIGHT * random.random)
'''

def getConnections(nodes, scope):
    connections = {}
    for n in range(len(nodes)):
        nConnections = []
        for m in range(n+1,len(nodes)):
            if nodes[n].coord.distance(nodes[m].coord) <= scope:
                nConnections.append(m)
        connections[n] = nConnections

    return connections

def flatenizeContacts(connections, nof_nodes, update_time):
    contacts = []
    for ts in range(len(connections)):
        for n in range(nof_nodes - 1):
            for m in connections[ts][n]:
                slots = 1
                for nts in range(ts + 1, len(connections)):
                    if m in connections[nts][n]:
                        connections[nts][n].remove(m)
                        slots += 1
                    else:
                        break
                contacts.append((ts*update_time,ts*update_time + slots*update_time, n+1, m+1))
                contacts.append((ts * update_time, ts * update_time + slots * update_time, m + 1, n + 1))

    return contacts


def genContactPlan(nof_nodes, map_size, end_time, update_interval, speed_range, wait_time_range, transmit_range, seed):
    # WORLD SIZE
    width = map_size[0]
    height = map_size[1]

    min_speed = speed_range[0]
    max_speed = speed_range[1]

    min_wait = wait_time_range[0]
    max_wait = wait_time_range[1]


    simTime = 0
    random.seed(a=seed)
    #set nodes initial position
    nodes = [Agent(Coord.getRandomCoord(width,height),width,height, min_speed,max_speed, min_wait, max_wait) for x in range(nof_nodes)]
    #nodes = [Agent(Coord(1,1)), Agent(Coord(2,2)), Agent(Coord(3,3))]

    #initialize simulation
    for a in nodes:
        a.setRandomDestinationAndSpeed()

    #warm up time

    connections = []
    while simTime < end_time:
#        print(str(simTime) + "\n")
#        for n in nodes:
#            print(str(n))
#        print("\n")
        #simTime to simTime+TIME_STEP period
        connections.append(getConnections(nodes,transmit_range))
        simTime += update_interval
        for a in nodes:
            a.move(update_interval)

    #print(connections)
    #finished simulation, get contacts
    contacts = flatenizeContacts(connections,nof_nodes, update_interval)
    return contacts
    #print(contacts)
    #print contact plan
    #for c in contacts:
        #print("a contact %f %f %d %d 1\n"% c)

    #Flatenizar contactos
    #Filtrar por duraciones menores
    #Print CP. (a contact ts te from to) (a contact ts te to from)

for seed in range(1, 12):
    for scope in sorted(list(range(0,56,5)) + [1]):
        cp = genContactPlan(4, (300,200), 5000, 1.0, (0.5,1.5), (0,0), scope, seed)
        with open("%s/contactPlant-ANTENNASCOPE=%d-SEED=%d.txt"%(OUTPUT_PATH,scope,seed), "w+") as text_file:
            for c in cp:
                text_file.write("a contact %f %f %d %d 1\n"% c)