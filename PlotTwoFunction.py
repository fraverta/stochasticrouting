import matplotlib.pyplot as plt
import ast

FILE_PATH_C1 = "/home/nando/Desktop/OneSetting/Test-4-nodes/function.txt"
FILE_PATH_C2 = "/home/nando/Desktop/Omnet-Result/functionOMNET.txt"

NAME_C1 = "Epidemic-ONE"
NAME_C2 = "Epidemic-DTNSim"

X_LABEL = "Antenna range in meters"
Y_LABEL = "Bundles received ratio"


FILE_OUTPUT_PATH = "/home/nando/Desktop/Epidemic-ONEvsDTNSim.png"

def main():
    c1 = getListFromFile(FILE_PATH_C1)
    c2 = getListFromFile(FILE_PATH_C2)

    # plot results
    line_up, = plt.plot([x[0] for x in c1],[y[1] for y in c1],'--o', label=NAME_C1)
    line_down, = plt.plot([x[0] for x in c2],[y[1] for y in c2],'--x', label=NAME_C2)
    plt.legend(handles=[line_up, line_down])
    plt.xlabel(X_LABEL)
    plt.ylabel(Y_LABEL)
    plt.grid(color='gray', linestyle='dashed')
    plt.savefig(FILE_OUTPUT_PATH)
    plt.clf()
    plt.cla()
    plt.close()

''''
Get a list from file reading first line only
'''
def getListFromFile(path):
    with open(path) as f:
        lines = f.readlines()

    return ast.literal_eval(lines[0])

main()