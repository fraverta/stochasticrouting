'''
To be used with ONE simulator report output.
Given SEED_RANGE functions, each one with ANTENNA_RANGE points this script compute the averague function and plot
it in a DELIVERY RATIO vs ANTENNA RANGE graph.
'''

import matplotlib.pyplot as plt
from functools import reduce

INPUT_PATH = "/home/nando/Desktop/OneSetting/Test-4-nodes"
OUTPUT_PATH = "/home/nando/Desktop/OneSetting/Test-4-nodes"

ANTENNA_RANGE = sorted([r for r in range(0,56,5)] + [1])
SEED_RANGE = [s for s in range(1,12)]

'''
Given a list of functions (list of pairs): [[(x0,y0),...] , [(xn,yn),....]]
returns a function computed as average of the above functions.
i.e. Given nof_repetions function with nof_points each one, it computes the average function
'''
def promList(llist, nof_repetitions, nof_points):
    assert len(llist) == nof_repetitions, "error nof_repetitions"
    assert len(list(filter(lambda l: len(l) != nof_points, llist))) == 0, "error nof_points"

    llist = [list(map(lambda f: f[x], llist)) for x in range(nof_points)] #extract all repetition values for 1 point (to compute average)
    # compute average for each point in two step: add and divide
    llist = list(map(lambda l: reduce(lambda x,y: (x[0] + y[0],x[1] + y[1]),l), llist))
    return [(x[0]/float(nof_repetitions), x[1]/float(nof_repetitions)) for x in llist]


def main():
    f_seed_list = []
    for seed in SEED_RANGE:
        f_seed = []
        for antenna in ANTENNA_RANGE:
            infile = open('%s/Test-4-nodes-ANTENNASCOPE=%i-SEED=%i_MessageStatsReport.txt'%(INPUT_PATH,antenna,seed),'r')
            infile.readline()
            report = dict([tuple(line[0:len(line)-1].split(": ")) for line in infile])
            f_seed.append((antenna,float(report['delivery_prob'])))

        f_seed_list.append(f_seed)
    f_prom = promList(f_seed_list,len(SEED_RANGE),len(ANTENNA_RANGE))

    # save function
    text_file = open("%s/functionONE.txt" % (OUTPUT_PATH), "w")
    text_file.write(str(f_prom))
    text_file.close()

    # plot results
    plt.plot([x[0] for x in f_prom], [y[1] for y in f_prom], '--o')
    plt.xlabel('Antenna range in meters')
    plt.ylabel('Delivery ratio')
    plt.grid(color='gray', linestyle='dashed')
    # xint = range(0, max_deleted_contacts + 1)
    # plt.xticks(xint)
    plt.savefig("%s/functionONE.png" % (OUTPUT_PATH))
    plt.clf()
    plt.cla()
    plt.close()

main()